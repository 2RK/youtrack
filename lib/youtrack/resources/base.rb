module Youtrack
  class Base
    include HTTParty

    # The base route URL
    attr_accessor :base_url

    # The Server Endpoint
    attr_accessor :service

    # Stores the response of the previous request
    attr_accessor :response

    def initialize(client)
      @service = client
      @base_url = @service.endpoint
      self.class.debug_output($stderr) if client.debug
    end

    protected

    def join(*args)
      File.join(*args)
    end

    def prepare_options(options = {})
      options[:headers] ||= {}
      if service.api_token.present?
        options[:headers]['Authorization'] = "Bearer #{service.api_token}"
      else
        options[:headers]['Cookie'] = service.cookies['Cookie']
      end
      options
    end

    def post(path, options = {})
      options = prepare_options(options)
      @response = self.class.post(join(base_url, path), options)
    end

    def put(path, options = {})
      options = prepare_options(options)
      @response = self.class.put(join(base_url, path), options)
    end

    def get(path, options = {})
      options = prepare_options(options)
      @response = self.class.get(join(base_url, path), options)
    end

    def delete(path, options = {})
      options = prepare_options(options)
      @response = self.class.delete(join(base_url, path), options)
    end

    private

    def format_data(record)
      new_record = {}

      record.each do |field_name, field_value|
        next if field_name == 'customFields'

        if field_value.is_a?(Hash)
          field_value.each do |sub_field_name, sub_field_value|
            if sub_field_name != '$type'
              new_record["#{field_name}#{sub_field_name}".downcase.gsub(' ', '')] = sub_field_value
            end
          end
        elsif field_value.is_a?(Array)
          new_record[field_name.downcase.gsub(' ', '')] = field_value.each_with_object([]) do |sub_record, record_list|
            record_list << format_data(sub_record)
          end
        else
          new_record[field_name.downcase.gsub(' ', '')] = field_value
        end
      end

      Array(record["customFields"]).each do |custom_field|
        if custom_field["value"].is_a?(Hash)
          new_record[custom_field["name"].downcase.gsub(' ', '')] = custom_field["value"]["minutes"] || custom_field["value"]["name"]
        else
          new_record[custom_field["name"].downcase.gsub(' ', '')] = custom_field["value"]
        end
      end

      new_record
    end
  end
end
