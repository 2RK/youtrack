module Youtrack
  class Project < Base
    # ==================
    # ADMIN Methods
    # ==================

    # filter        string    Apply a filter to issues in a project.
    # after         Integer   A number of issues to skip before getting a list of issues. That is, when you specify, for example, after=12 in request,
    #                         then in the response you will get all issues matching request but without first twelve issues found .
    # max           Integer   Maximum number of issues to be imported. If not provided, 10 issues will be imported, by default.
    # updatedAfter  Long      Filter issues by the date of the most recent update. Only issues imported after the specified date will be gotten.
    def get_issues_for(project_id, params)
      get(path("admin/projects/#{project_id}/issues", params))
      response.parsed_response
    end

    def all(params)
      get(path('admin/projects', params))
      response.parsed_response
    end

    def find(project_id)
      get("admin/projects/#{project_id}")
      response.parsed_response
    end

    # required attributes
    #
    # projectId  string required Unique identifier of a project to be created. This short name will be used as prefix in issue IDs for this project.
    # projectName  string required Full name of a new project. Must be unique.
    # startingNumber   integer  required Number to assign to the next manually created issue.
    # projectLeadLogin   string required Login name of a user to be assigned as a project leader.
    # description  string  Optional description of the new project
    def create(attributes={})
      put("admin/projects/#{attributes[:projectId]}", query: attributes)
      response
    end

    def destroy(project_id)
      delete("admin/projects/#{project_id}")
      response
    end

    private

    def path(api_path, params)
      uri = URI(api_path)
      uri.query = URI.encode_www_form(params) if params.present?

      uri.to_s
    end
  end
end
